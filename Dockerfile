# FROM python:3

# WORKDIR /usr/src/app

# COPY requirements.txt ./
# RUN pip3 install --no-cache-dir -r requirements.txt
FROM ubuntu:latest


WORKDIR /usr/src/app

#update yum
RUN apt-get update

#install python
RUN apt-get install python3.6 -y

#install pip to download library
RUN apt install python3-pip -y

#install scrapy to crawl web data
RUN pip3 install scrapy

COPY . .

CMD [ "python3", "./go-spider.py" ]
