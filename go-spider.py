from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

#from mycrawler.spiders.pageavailability import PageavailabilitySpider
from mycrawler.spiders.techinsight import TechinsightTableSpider

process = CrawlerProcess(get_project_settings())
process.crawl(TechinsightTableSpider)
process.start()
