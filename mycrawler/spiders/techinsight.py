# -*- coding: utf-8 -*-

import scrapy

from scrapy.selector import Selector


class TechinsightTableSpider(scrapy.Spider):
    name = 'techinsight'

    allowed_domains = ['https://techinsight.com.vn/category/chuyen-gia-viet/cloud-computing-vi/']

    start_urls = ['https://techinsight.com.vn/category/chuyen-gia-viet/cloud-computing-vi/']

    custom_settings = {
        'LOG_FILE': 'logs/techinsighttable.log',
        'LOG_LEVEL': 'INFO'
    }
    def parse(self, response):
        print("procesing:"+response.url)

        title = Selector(response).xpath(
            '//div[@class="td-block-span4"]/div/h3/a/text()').extract()

        image = Selector(response).xpath(
            '//div[@class="td-block-span4"]/div/div[@class="td-module-image"]/div[@class="td-module-thumb"]/a').extract()

        content = Selector(response).xpath(
            '//div[@class="td-block-span4"]/div/div[@class="td-excerpt"]/text()').extract()

        for item in zip(title, image, content):

            scraped_info = {

                'page': response.url,

                'title': item[0],

                'image': item[1],

                'content': item[2]

            }
            yield scraped_info
