from scrapy.spiders import SitemapSpider


class SitemapurlSpider(SitemapSpider):
    name = 'sitemapurl'
    # Replace the sitemap xml URL with a real one.
    sitemap_urls = ['https://techinsight.com.vn/category/chuyen-gia-viet/cloud-computing-vi/']
    custom_settings = {
        'LOG_FILE': 'logs/techinsight.log',
        'LOG_LEVEL': 'INFO'
    }

    def parse(self, response):
        pass
